# Copyright 2020 - The turbulenza authors
#
# Generate a publications.html file that includes 'archival' and 'other'
# sections automatically built from two BiBTeX files located under
# data/archival.bib and data/other.bib, respectively.
#
# The result is concatenated with two existing html files as follows:
# - publications_preamble.html
# - generated HTML code containing the archival and other publications sections
# - publications_postamble.html 
# into the resulting:
# - publications.html
# file.

from pybtex.database.input import bibtex

preamble_file = open("publications_preamble.html", 'r')
preamble = preamble_file.read()
preamble_file.close()
postamble_file = open("publications_postamble.html", 'r')
postamble = postamble_file.read()
postamble_file.close()

output_file = open("publications.html", 'w')
output_file.write(preamble)

output_file.write("<h2>Archival Publications</h2>")
output_file.write("\n<ol reversed>")
archival_bib = bibtex.Parser().parse_file('data/archival.bib')
for paper in archival_bib.entries:
  entry_string = "\n<li>\n  "
  for adx, author in enumerate(archival_bib.entries[paper].persons['author']):
    first_name = [elem[0] + "." for elem in author.first_names]
    last_name = [elem for elem in author.last_names]
    #print("first_name", first_name)
    #print("last_name", last_name)
    author_string = "".join(first_name) + " " + "".join(last_name)
    entry_string += author_string
    if adx < len(archival_bib.entries[paper].persons['author'])-1:
      entry_string += ", "
      
  entry_string += "\n  (" + archival_bib.entries[paper].fields['year'] + ")"
  entry_string += "\n  <b>" + archival_bib.entries[paper].fields['title'] + "</b>"
  if "journal" in archival_bib.entries[paper].fields:
    entry_string += "\n  <em>" + archival_bib.entries[paper].fields['journal'] + "</em>"
  elif "booktitle" in archival_bib.entries[paper].fields:
    entry_string += "\n  in <em>" + archival_bib.entries[paper].fields['booktitle'] + "</em>"
  if  "volume" in archival_bib.entries[paper].fields:
    entry_string += "\n  " + archival_bib.entries[paper].fields['volume']
  if  "number" in archival_bib.entries[paper].fields:
    entry_string += "(" + archival_bib.entries[paper].fields['number'] + ")"
  if  "doi" in archival_bib.entries[paper].fields:
    entry_string += "\n  <a style=\"text-decoration:none\" href=\"" + archival_bib.entries[paper].fields['doi'] + \
                    "\" target=\"_blank\" rel=\"noreferrer noopener\">"
    entry_string += " " + archival_bib.entries[paper].fields['doi'] + "</a>,"
  entry_string += "\n</li>"
  output_file.write(entry_string)
output_file.write("</ol>")

#output_file.write("\n</p>\n<hr></hr>\n<h2>Other</h2>")
output_file.write("\n<h2>Other</h2>")
output_file.write("<p>")
output_file.write("<ol reversed>")
other_bib = bibtex.Parser().parse_file('data/other.bib')
for paper in other_bib.entries:
  entry_string = "\n<li>\n  "
  print("paper", paper)
  for adx, author in enumerate(other_bib.entries[paper].persons['author']):
    first_name = [elem[0] + "." for elem in author.first_names]
    last_name = [elem for elem in author.last_names]
    #print("first_name", first_name)
    #print("last_name", last_name)
    author_string = "".join(first_name) + " " + "".join(last_name)
    entry_string += author_string
    if adx < len(other_bib.entries[paper].persons['author'])-1:
      entry_string += ", "
      
  entry_string += "\n  (" + other_bib.entries[paper].fields['year'] + ")"
  entry_string += "\n  <b>" + other_bib.entries[paper].fields['title'] + "</b>"
  if "journal" in other_bib.entries[paper].fields:
    entry_string += "\n  <em>" + other_bib.entries[paper].fields['journal'] + "</em>"
  elif "booktitle" in other_bib.entries[paper].fields:
    entry_string += "\n  in <em>" + other_bib.entries[paper].fields['booktitle'] + "</em>"
  if  "volume" in other_bib.entries[paper].fields:
    entry_string += "\n  " + other_bib.entries[paper].fields['volume']
  if  "number" in other_bib.entries[paper].fields:
    entry_string += "(" + other_bib.entries[paper].fields['number'] + ")"
  if  "doi" in other_bib.entries[paper].fields:
    entry_string += "\n  <a style=\"text-decoration:none\" href=\"" + other_bib.entries[paper].fields['doi'] + \
                    "\" target=\"_blank\" rel=\"noreferrer noopener\">"
    entry_string += " " + other_bib.entries[paper].fields['doi'] + "</a>,"
  entry_string += "\n</li>"
  output_file.write(entry_string)
output_file.write("</ol>\n")

output_file.write(postamble)
output_file.close()
